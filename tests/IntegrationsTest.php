<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_dinos(){
        $response = $this->make_request("GET", "/");
        $dinos = getDinos();
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        foreach($dinos as $dino){
            $this->assertContains($dino->name, $body);
            $this->assertContains($dino->avatar, $body);
        }
    }

    public function test_single_dino(){
        $response = $this->make_request("GET", "/dinosaur/triceratops");
        $dino_info = getDinoInfo("triceratops");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("triceratops", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);

    }  
}
?>