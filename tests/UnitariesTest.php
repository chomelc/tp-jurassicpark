<?php require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {

    public function test_getDinos(){
        $dinos = getDinos();
        $this->assertInternalType('array',$dinos);
        $this->assertEquals(7, count($dinos));
        $this->assertInternalType('string',$dinos[0]->avatar);
    }


    public function test_getDinoInfo(){
        $dino = getDinoInfo("triceratops");
        $this->assertInstanceOf(stdClass::class,$dino);
        $this->assertInternalType('string',$dino->avatar);
    }

    public function test_randomDinos(){
        $top_dinos = randomDinos();
        $this->assertInternalType('array',$top_dinos);
        $this->assertInternalType('string',$top_dinos[0]->avatar);
    }

}
?>