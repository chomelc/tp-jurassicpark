## TP-JurassicPark 🦖

### About

A "simple" repo for a web development class' assesment.

___

### Requirements

```
mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown
```

### Author

> Clémence CHOMEL
