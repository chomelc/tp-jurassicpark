<?php
function getDinos(){
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getDinoInfo($slug) {
    $path = sprintf("https://allosaurus.delahayeyourself.info/api/dinosaurs/%s", $slug);
    $response = Requests::get($path);
    return json_decode($response->body);
}

function randomDinos(){
    $dinos = getDinos();
    $keys = array_rand($dinos, 3);
    $top_dinos = array($dinos[$keys[0]],$dinos[$keys[1]],$dinos[$keys[2]]);

    return $top_dinos;
}

?>